import os
from time import sleep

import cv2
import pytesseract
from PIL import Image
from selenium import webdriver

from util.check_driver import check_driver


def crop_image(img, y, x, h, w):
    image = cv2.imread(img)
    height, width, channels = image.shape

    a = round(height * y)
    b = round(height * h)
    c = round(width * x)
    d = round(width * w)

    return image[a:a + b, c:c + d]


def save_image(img, folder, filename, lib):
    path = os.path.join(os.getcwd(), folder, '{}.jpg'.format(filename))

    if lib == 'cv2':
        cv2.imwrite(path, img)
    elif lib == 'pil':
        img.save(path)
    else:
        return

    return path


def rotate_image(img, angle):
    color_image = Image.open(img)
    transposed = color_image.transpose(eval('Image.ROTATE_{}'.format(angle)))

    return transposed


def tesseract(img, lang='por'):
    image = cv2.imread(img)
    result = pytesseract.image_to_string(image, lang=lang)

    return result


def ocr_online(img):
    # Check if driver exist inside folder app
    check_driver()
    # Start driver of browser
    options = webdriver.ChromeOptions()
    options.add_argument("--incognito")
    options.add_argument('--headless')
    chrome = webdriver.Chrome("chromedriver", options=options)

    chrome.get('https://www.invertexto.com/ocr-online')

    elm = chrome.find_element_by_xpath('//input[@type="file"]')
    elm.send_keys(img)

    while chrome.find_element_by_name('destino').get_attribute('value') == '':
        sleep(1)

    result = chrome.find_element_by_name('destino').get_attribute('value')

    chrome.quit()

    return result.strip()
