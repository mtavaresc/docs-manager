import requests


class Yandex:
    def __init__(self):
        self.key = 'trnsl.1.1.20190606T173316Z.13c630475c13c38a.797e183b622871a0ffa82cdc6bc57822f873af3e'

    def translate(self, lang, text):
        url = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
        response = requests.get(url, data={'key': self.key, 'text': text, 'lang': lang})
        result = eval(response.text)
        for ele in result:
            key = ele
            val = result[ele]
            if key == 'text':
                return val[0]


if __name__ == '__main__':
    t = Yandex().translate(lang='en-pt', text='Birthplace')
    print(t)
