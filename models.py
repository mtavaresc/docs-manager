from base import db
from datetime import date


class Client(db.Model):
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    cnpj = db.Column(db.String(14), unique=True)
    social_name = db.Column(db.String(150), nullable=False)
    cleaning_frequency_in_days = db.Column(db.Integer)
    last_cleaning_date = db.Column(db.Date)

    documents = db.relationship('Document', cascade='all, delete-orphan')
    payments = db.relationship('Payment', cascade='all, delete-orphan')

    def __repr__(self):
        return '<Client {}>'.format(self.cnpj)

    def __init__(self, cnpj, social_name, cleaning_frequency=0, last_cleaning=date.today()):
        self.cnpj = cnpj
        self.social_name = social_name
        self.cleaning_frequency_in_days = cleaning_frequency
        self.last_cleaning_date = last_cleaning


class Layout(db.Model):
    """ Table used for setting templates  """
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name = db.Column(db.String(30), unique=True)
    message = db.Column(db.String(150, collation='utf8_bin'), nullable=False)
    multiple = db.Column(db.Boolean, nullable=False)
    category = db.Column(db.String(1), nullable=False)

    def __repr__(self):
        return '<Layout {}>'.format(self.name)

    def __init__(self, name, message, category, m=False):
        self.name = name
        self.message = message
        self.category = category
        self.multiple = m


class Document(db.Model):
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    template = db.Column(db.String(10))  # Name Document Template
    client = db.Column(db.String(14), db.ForeignKey('client.cnpj'), nullable=False)
    document = db.Column(db.Integer, nullable=False)  # Id Document Template
    entry_date = db.Column(db.Date, nullable=False)

    db.UniqueConstraint(template, client, document, name='uc_document')

    def __init__(self, template, client, document, entry_date=date.today()):
        self.template = template
        self.client = client
        self.document = document
        self.entry_date = entry_date


class Payment(db.Model):
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    competence = db.Column(db.Date)
    value = db.Column(db.DECIMAL(11, 2))
    template = db.Column(db.String(10))  # Name Payment Template
    client = db.Column(db.String(14), db.ForeignKey('client.cnpj'), nullable=False)
    payment = db.Column(db.Integer, nullable=False)  # Id Title Payment Template
    entry_date = db.Column(db.Date, nullable=False)

    db.UniqueConstraint(template, client, payment, name='uc_payment')

    def __init__(self, competence, value, template, client, payment, entry_date=date.today()):
        self.competence = competence
        self.value = value
        self.template = template
        self.client = client
        self.payment = payment
        self.entry_date = entry_date


class Cagece(db.Model):
    """ Payment Title """
    __tablename__ = 't_cagece'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    client_name = db.Column(db.String(90), nullable=False)
    client_address = db.Column(db.String(200))
    competence = db.Column(db.Date, nullable=False)
    maturity = db.Column(db.Date)
    value = db.Column(db.DECIMAL(11, 2))
    db.UniqueConstraint(client_name, competence, name='uc_cagece')

    def __init__(self, client_address, client_name, competence, value, maturity):
        self.client_name = client_name
        self.client_address = client_address if client_address != 'None' else None
        self.competence = competence
        self.maturity = maturity if maturity != 'None' else None
        self.value = value if value != 'None' else None


class Cnh(db.Model):
    """ Personal Document """
    __tablename__ = 't_cnh'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    cpf = db.Column(db.String(11), unique=True, nullable=False)
    document = db.Column(db.String(200))
    membership = db.Column(db.String(200))
    birthday = db.Column(db.Date)
    name = db.Column(db.String(90))
    first_license = db.Column(db.Date)
    license_number = db.Column(db.String(11))
    validity = db.Column(db.Date)

    def __init__(self, cpf, document, membership, birthday, name, license_number, first_license, validity):
        self.cpf = cpf
        self.document = document if document != 'None' else None
        self.membership = membership if membership != 'None' else None
        self.birthday = birthday if birthday != 'None' else None
        self.name = name if name != 'None' else None
        self.first_license = first_license if first_license != 'None' else None
        self.license_number = license_number if license_number != 'None' else None
        self.validity = validity if validity != 'None' else None


class Clt(db.Model):
    """ Personal Document """
    __tablename__ = 't_clt'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name = db.Column(db.String(90), unique=True, nullable=False)
    birthplace = db.Column(db.String(90))
    membership = db.Column(db.String(200))
    document = db.Column(db.String(200))
    cnh = db.Column(db.String(11))
    voter_number = db.Column(db.String(11))
    voter_section = db.Column(db.String(3))
    voter_zone = db.Column(db.String(4))
    birthday = db.Column(db.Date)
    cpf = db.Column(db.String(11))

    def __init__(self, cnh, cpf, document, membership, birthplace, birthday, name, voter_number, voter_section,
                 voter_zone):
        self.name = name
        self.birthplace = birthplace if birthplace != 'None' else None
        self.membership = membership if membership != 'None' else None
        self.document = document if document != 'None' else None
        self.cnh = cnh if cnh != 'None' else None
        self.voter_number = voter_number if voter_number != 'None' else None
        self.voter_section = voter_section if voter_section != 'None' else None
        self.voter_zone = voter_zone if voter_zone != 'None' else None
        self.birthday = birthday if birthday != 'None' else None
        self.cpf = cpf if cpf != 'None' else None


class Rg(db.Model):
    """ Personal Document """
    __tablename__ = 't_rg'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    registration_number = db.Column(db.String(9), unique=True, nullable=False)
    expedition_date = db.Column(db.Date)
    name = db.Column(db.String(90))
    membership = db.Column(db.String(200))
    birthplace = db.Column(db.String(90))
    birthday = db.Column(db.Date)
    cpf = db.Column(db.String(11))

    def __init__(self, cpf, expedition_date, birthday, membership, birthplace, name, registration_number):
        self.registration_number = registration_number
        self.expedition_date = expedition_date if expedition_date != 'None' else None
        self.name = name if name != 'None' else None
        self.membership = membership if membership != 'None' else None
        self.birthplace = birthplace if birthplace != 'None' else None
        self.birthday = birthday if birthday != 'None' else None
        self.cpf = cpf if cpf != 'None' else None


class Cpf(db.Model):
    """ Personal Document """
    __tablename__ = 't_cpf'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    registration_number = db.Column(db.String(11), unique=True, nullable=False)
    name = db.Column(db.String(90))
    birthday = db.Column(db.Date)

    def __init__(self, birthday, name, registration_number):
        self.registration_number = registration_number
        self.name = name if name != 'None' else None
        self.birthday = birthday if birthday != 'None' else None


class Cfe(db.Model):
    """ Payment Title """
    __tablename__ = 't_cfe'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    number_id = db.Column(db.String(9), unique=True, nullable=False)
    competence = db.Column(db.Date)
    value = db.Column(db.DECIMAL(11, 2))
    emitter = db.Column(db.String(14))
    consumer = db.Column(db.String(14))

    def __init__(self, emitter, consumer, competence, sat, value):
        self.number_id = sat
        self.competence = competence if competence != 'None' else None
        self.value = value if value != 'None' else None
        self.emitter = emitter if emitter != 'None' else None
        self.consumer = consumer if consumer != 'None' else None


class Boleto(db.Model):
    """ Payment Title """
    __tablename__ = 't_boleto'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    bank_code = db.Column(db.String(3))
    bar_code = db.Column(db.String(47), unique=True, nullable=True)
    payment_place = db.Column(db.String(90))
    beneficiary = db.Column(db.String(90))
    beneficiary_id = db.Column(db.String(14))
    competence = db.Column(db.Date)
    value = db.Column(db.DECIMAL(11, 2))

    def __init__(self, beneficiary, beneficiary_id, bank, bar, competence, place, value):
        self.bank_code = bank[:3] if bank != 'None' else None
        self.bar_code = bar
        self.payment_place = place if place != 'None' else None
        self.beneficiary = beneficiary if beneficiary != 'None' else None
        self.beneficiary_id = beneficiary_id if beneficiary_id != 'None' else None
        self.competence = competence if competence != 'None' else None
        self.value = value if value != 'None' else None


class Enel(db.Model):
    """ Payment Title """
    __tablename__ = 't_enel'

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    client_id = db.Column(db.String(8), nullable=False)
    client_name = db.Column(db.String(90))
    client_address = db.Column(db.String(200))
    competence = db.Column(db.Date, nullable=False)
    value = db.Column(db.DECIMAL(11, 2))
    db.UniqueConstraint(client_id, competence, name='uc_enel')

    def __init__(self, competence, client_address, client_name, client_id, value):
        self.client_id = client_id
        self.client_name = client_name if client_name != 'None' else None
        self.client_address = client_address if client_address != 'None' else None
        self.competence = competence
        self.value = value if value != 'None' else None
