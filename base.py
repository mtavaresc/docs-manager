from datetime import timedelta
from os import path

from flask import Flask
from flask_dropzone import Dropzone
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, static_folder='static')
app.secret_key = b'\xa7*\xb2F\x0e\x8a\xf5a\x91\x8ev0\xa6\xc5 \xd6'
app.permanent_session_lifetime = timedelta(minutes=20)

# Schema default
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://developer:123456@localhost:3306/docs?charset=utf8"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.config["SQLALCHEMY_COMMIT_ON_TEARDOWN"] = False

db = SQLAlchemy(app)

# Dropzone settings
dropzone = Dropzone(app)
app.config["UPLOADED_PATH"] = path.join(app.static_folder, 'samples')
app.config['DROPZONE_IN_FORM'] = True
# app.config['DROPZONE_UPLOAD_MULTIPLE'] = False
app.config['DROPZONE_ALLOWED_FILE_CUSTOM'] = True
app.config['DROPZONE_ALLOWED_FILE_TYPE'] = 'image/*'
app.config['DROPZONE_UPLOAD_ON_CLICK'] = True
app.config['DROPZONE_UPLOAD_ACTION'] = 'handle_upload'
app.config['DROPZONE_UPLOAD_BTN_ID'] = 'enviar'
app.config['DROPZONE_INVALID_FILE_TYPE'] = 'Você não pode enviar este tipo de arquivo.'
app.config['DROPZONE_FILE_TOO_BIG'] = 'Arquivo muito grande {{filesize}} MB. Tamanho máximo: {{maxFilesize}} MB.'
# app.config["DROPZONE_DEFAULT_MESSAGE"] = 'Arraste o arquivo XML aqui ou clique para procurar'
