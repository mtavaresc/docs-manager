from datetime import date, datetime, timedelta
from glob import glob
from logging import basicConfig, info, DEBUG
from os import path, remove
from time import sleep

from termcolor import cprint

from models import Client, db


def print_cyan(text, log_info):
    if log_info:
        info(text)

    return cprint(text, 'cyan')


def to_clean():
    basicConfig(filename='application.log', level=DEBUG)
    now = datetime.now().replace(microsecond=0).strftime('%d/%m/%Y %H:%M:%S')
    clients = Client.query.all()

    print_cyan('\t* Cleaning Service - {now}\n'.format(now=now), True)

    for client in clients:
        today = date.today()
        next_cleaning_date = client.last_cleaning_date + timedelta(client.cleaning_frequency_in_days)

        if today == next_cleaning_date:
            for x in glob(path.join('static', 'samples', client.cnpj, '**', '*.*'), recursive=True):
                remove(x)

            print_cyan('\t* The cleaning day has arrived! - {now}'.format(now=now), True)
            print_cyan('\t* Client: {client} - {social}\n'.format(client=client.cnpj, social=client.social_name), True)

            Client.query.filter_by(cnpj=client.cnpj).update({'last_cleaning_date': today})
            db.session.commit()


def run():
    while True:
        to_clean()
        sleep(2 * 60 * 60)


if __name__ == '__main__':
    run()
