import re
from datetime import datetime
from glob import glob
from json import dumps

from util.tools import *


def if_not_exist_create(*folders):
    for folder in folders:
        if not os.path.isdir(folder):
            os.mkdir(folder)


def search_pattern(regex, text):
    return re.search(regex, text).group(0) if re.search(regex, text) is not None else ''


def str_to_float(string):
    return float(string.replace(',', '.'))


def str_to_date(string, f='%d/%m/%Y', t='%Y-%m-%d'):
    return datetime.strptime(string, f).strftime(t)


class Template:
    def __init__(self, client, layout, dump=False):
        self.client = client
        self.layout = layout
        self.dumps = dump

    def execute(self):
        samples = os.path.join(os.getcwd(), 'static', 'samples')
        folder_client = os.path.join(samples, self.client)
        folder_layout = os.path.join(folder_client, self.layout)
        cutouts = os.path.join(folder_layout, 'cutouts')
        if_not_exist_create(samples, folder_client, folder_layout)

        if self.layout == 'cagece':
            name = ''
            address = ''
            competence = ''
            expiration = ''
            valor = ''

            for sample in glob(os.path.join(folder_layout, '*.*')):
                res = ocr_online(sample)
                if res == 'Nenhum texto encontrado.':
                    res = tesseract(sample)

                lines = res.splitlines()

                for line in lines:
                    if 'Nome' in line:
                        name = line.replace('Nome', '').strip()
                    elif 'End. LeituP' in line:
                        address = line.replace('End. LeituP', '').strip()
                    elif 'MÉS/ANO' in line:
                        competence = lines[lines.index('MÉS/ANO') + 3]
                    elif 'VENCIMENTO' in line:
                        expiration = lines[lines.index('VENCIMENTO') + 3]
                    elif 'Total (RS):' in line:
                        valor = line.replace('Total (RS):', '').strip()

                os.remove(sample)

            dados_cagece = {
                'Layout': 'CAGECE',
                'Cliente': {'Nome': name, 'Endereço': address},
                'Pagamento': {'Mês/Ano': competence, 'Vencimento': expiration, 'Valor': valor}

            }

            if self.dumps:
                print(dumps(dados_cagece, indent=4))

            return dados_cagece

        elif self.layout == 'cnh':
            if_not_exist_create(cutouts)
            cpf = ''
            doc = ''
            fili = ''
            nasc = ''
            nome = ''
            prihab = ''
            regist = ''
            valid = ''

            cnh_zones = [
                {'zone': 'nome', 'y': 0.12, 'x': 0.15, 'h': 0.03, 'w': 0.78},
                {'zone': 'doc', 'y': 0.17, 'x': 0.49, 'h': 0.02, 'w': 0.44},
                {'zone': 'cpf', 'y': 0.22, 'x': 0.5, 'h': 0.02, 'w': 0.26},
                {'zone': 'nasc', 'y': 0.22, 'x': 0.76, 'h': 0.02, 'w': 0.18},
                {'zone': 'fili', 'y': 0.27, 'x': 0.5, 'h': 0.1, 'w': 0.43},
                {'zone': 'regist', 'y': 0.45, 'x': 0.16, 'h': 0.03, 'w': 0.3},
                {'zone': 'valid', 'y': 0.46, 'x': 0.49, 'h': 0.02, 'w': 0.19},
                {'zone': 'prihab', 'y': 0.46, 'x': 0.71, 'h': 0.02, 'w': 0.22},
            ]
            for sample in glob(os.path.join(folder_layout, '*.*')):
                for zone in cnh_zones:
                    crop = crop_image(sample, y=zone['y'], x=zone['x'], h=zone['h'], w=zone['w'])
                    save_image(crop, cutouts, zone['zone'], 'cv2')

                for file in glob(os.path.join(cutouts, '*.*')):
                    base = os.path.basename(file)

                    res = ocr_online(file)
                    if res == 'Nenhum texto encontrado.':
                        res = tesseract(file)

                    os.remove(file)

                    zone = os.path.splitext(base)[0].strip()

                    if zone == 'cpf':
                        cpf = re.sub(r'[\s.\-/]', '', res)
                    elif zone == 'doc':
                        doc = re.findall(r'\d+', res)[0]
                    elif zone == 'fili':
                        fili = res.replace('\n', ' ')
                    elif zone == 'nasc':
                        nasc = res
                    elif zone == 'nome':
                        lst = re.findall(r'\b[A-Z][A-Z]+\b', res)
                        nome = ' '.join([w for w in lst])
                    elif zone == 'prihab':
                        prihab = res
                    elif zone == 'regist':
                        regist = res
                    elif zone == 'valid':
                        valid = res

                os.remove(sample)

            dados_cnh = {
                'Layout': 'CNH',
                'CPF': cpf,
                'Doc. Identidade': doc,
                'Filiação': fili,
                'Nascimento': nasc,
                'Nome': nome,
                'Primeira Habilitação': prihab,
                'Nº Registro': regist,
                'Validade': valid
            }

            if self.dumps:
                print(dumps(dados_cnh, indent=4))

            return dados_cnh

        elif self.layout == 'clt':
            if_not_exist_create(cutouts)
            name = ''
            loc_nasc = ''
            fili = ''
            doc = ''
            reg_cnh = ''
            tit = ''
            secao = ''
            zona = ''
            nasc = ''
            cpf = ''

            for sample in glob(os.path.join(folder_layout, '*.*')):
                crop = crop_image(sample, y=0.13, x=0.11, h=0.81, w=0.38)
                s = save_image(crop, cutouts, self.layout, 'cv2')

                rotate = rotate_image(s, angle=270)
                r = save_image(rotate, cutouts, '{}_270'.format(self.layout), lib='pil')

                res = ocr_online(r)

                lines = res.splitlines()

                if 'APRESENTADO:' not in lines[4]:
                    mom = ' E {0}'.format(lines[4])
                else:
                    mom = ''

                for line in lines:
                    if 'NOME:' in line:
                        name = line.replace('NOME:', '').strip()
                    elif 'LOC. DE NASC' in line:
                        loc_nasc = line.split(':')[1].strip()
                    elif 'FILIAÇÃO' in line:
                        fili = '{dad}{mom}'.format(dad=line.split(':')[1], mom=mom).strip()
                    elif 'APRESENTADO:' in line:
                        doc = line.split(':')[1].strip()
                    elif 'CNH' in line:
                        reg_cnh = line.replace('CNH', '').strip()
                    elif 'TIT. ELEITOR:' in line:
                        digits = re.findall(r'\d+', line)
                        if len(digits) > 2:
                            tit = digits[0]
                            secao = digits[1]
                            zona = digits[2]
                        elif len(digits) > 1:
                            tit = digits[0]
                            secao = digits[1]
                        else:
                            tit = digits[0]
                    elif 'SEÇÃO:' in line:
                        secao = line.replace('SEÇÃO:', '').strip()
                    elif 'ZONA:' in line:
                        zona = line.replace('ZONA:', '').strip()
                    elif 'NASCIMENTO:' in line:
                        nasc = line.replace('NASCIMENTO:', '').strip()
                    elif 'CPF:' in line:
                        ncpf = line.replace('CPF:', '').strip()
                        cpf = re.sub(r'[\s.\-/]', '', ncpf)

                os.remove(sample)

            dados_clt = {
                'Layout': 'CLT',
                'Nome': name,
                'Local de Nascimento': loc_nasc,
                'Filiação': fili,
                'Doc. Apresentado': doc,
                'CNH': reg_cnh,
                'Título de Eleitor': {'Número': tit, 'Seção': secao, 'Zona': zona},
                'Nascimento': nasc,
                'CPF': cpf
            }

            if self.dumps:
                print(dumps(dados_clt, indent=4))

            return dados_clt

        elif self.layout == 'rg':
            registration_number = ''
            expedition_date = ''
            name = ''
            membership = ''
            birthplace = ''
            birthday = ''
            cpf = ''

            for sample in glob(os.path.join(folder_layout, '*.*')):
                res = ocr_online(sample)

                l_cpf = re.findall(r'([0-9]{3}[.]?[0-9]{3}[.]?[0-9]{3}[-]?[0-9]{2})', res)
                cpf = l_cpf[0] if len(l_cpf) > 0 else ''
                dates = re.findall(r'([0-3][0-9][/][0-1][0-9][/][0-9]{4})', res)
                if len(dates) > 1:
                    expedition_date = dates[0]
                    birthday = dates[1]
                else:
                    expedition_date = dates[0]

                lines = res.splitlines()
                for line in lines:
                    if 'REGISTRO' in line:
                        registration_number = re.sub(r'[\s.\-/]', '', line.replace('REGISTRO', ''))
                    elif 'NOME' in line:
                        name = lines[lines.index(line) + 1]
                        membership = '{dad} E {mom}'.format(dad=lines[lines.index(line) + 3],
                                                            mom=lines[lines.index(line) + 4])
                    elif line == birthday:
                        birthplace = lines[lines.index(line) + 1]

                os.remove(sample)

            dados_rg = {
                'Layout': 'RG',
                'Registro Geral': registration_number,
                'Data de Expedição': expedition_date,
                'Nome': name,
                'Filiação': membership,
                'Naturalidade': birthplace,
                'Data de Nascimento': birthday,
                'CPF': cpf
            }

            if self.dumps:
                print(dumps(dados_rg, indent=4))

            return dados_rg

        elif self.layout == 'cpf':
            registration_number = ''
            name = ''
            birthday = ''

            for sample in glob(os.path.join(folder_layout, '*.*')):
                res = ocr_online(sample)

                cpf = re.findall(r'([0-9]{3}[.]?[0-9]{3}[.]?[0-9]{3}[-]?[0-9]{2})', res)
                registration_number = re.sub(r'[\s.\-/]', '', cpf[0]) if len(cpf) > 0 else ''
                l_birthday = re.findall(r'([0-9]{2}[/]?[0-9]{2}[/]?[0-9]{4})', res)
                birthday = l_birthday[0] if len(l_birthday) > 0 else ''

                lines = res.splitlines()
                for line in lines:
                    if 'Nome' in line:
                        name = lines[lines.index(line) + 1]
                    elif 'Número de Inscrição' in line and registration_number == '':
                        registration_number = ''.join(re.findall(r'\d+', lines[lines.index(line) + 1]))

                os.remove(sample)

            dados_cpf = {
                'Layout': 'CPF',
                'Número de Inscrição': registration_number,
                'Nome': name,
                'Nascimento': birthday
            }

            if self.dumps:
                print(dumps(dados_cpf, indent=4))

            return dados_cpf

        elif self.layout == 'cfe':
            sat_number = ''
            competence = ''
            amount = ''
            emitter = ''
            consumer = ''

            for sample in glob(os.path.join(folder_layout, '*.*')):
                res = ocr_online(sample)

                lines = res.splitlines()
                for line in lines:
                    if 'CNPJ:' in line:
                        emitter = search_pattern(r'([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})', line)
                    elif 'TOTAL R$' in line:
                        amount = lines[lines.index(line) + 1]
                    elif 'SAT N' in line:
                        sat_line = search_pattern(r'([0-9]{9})', line)
                        sat_number = lines[lines.index(line) + 1] if sat_line == '' else sat_line
                        competence = lines[lines.index(line) + 2] if sat_line == '' else lines[lines.index(line) + 1]
                    elif 'CPF/CNPJ Consumidor:' in line:
                        pattern = r'([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})' \
                                  r'|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})'
                        consumer = search_pattern(pattern, line)

                # os.remove(sample)

            dados_cfe = {
                'Layout': 'CFe',
                'SAT Nº': sat_number,
                'Data/Hora Emissão': competence,
                'Valor Total': amount,
                'CNPJ Emissor': emitter,
                'CPF/CNPJ Consumidor': consumer
            }

            if self.dumps:
                print(dumps(dados_cfe, indent=4))

            return dados_cfe

        elif self.layout == 'boleto':
            bank_code = ''
            barcode = ''
            payment_place = ''
            beneficiary = ''
            cnpj_beneficiary = ''
            competence = ''
            value = ''

            for sample in glob(os.path.join(folder_layout, '*.*')):
                res = ocr_online(sample)

                l_dates = re.findall(r'([0-3][0-9][/][0-1][0-9][/][0-9]{4})', res)
                competence = l_dates[0] if len(l_dates) > 0 else ''
                l_values = list(map(str_to_float, re.findall(r'(\d+,[0-9]{2})', res)))
                value = '{:.2f}'.format(max(l_values)).replace('.', ',') if len(l_values) > 0 else ''
                l_barcode = re.findall(r'([0-9]{5}[.]?[0-9]{5}[\s]?[0-9]{5}[.]?[0-9]{6}[\s]?[0-9]{5}[.][0-9]{6}[\s]?'
                                       r'[0-9][\s]?[0-9]{14})|([0-9]{47})', res)
                barcode = l_barcode[0][0] if len(l_barcode) > 0 else ''
                l_banks = re.findall(r'([0-9]{3}[\-][0-9])', res)
                bank_code = l_banks[0] if len(l_banks) > 0 else ''

                lines = res.splitlines()
                for line in lines:
                    if 'Local de pagamento' in line:
                        payment_place = lines[lines.index(line) + 2].strip()
                    elif 'Beneficiario' in line:
                        beneficiary = lines[lines.index(line) + 3].strip()
                        pattern = r'([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})' \
                                  r'|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})'
                        cnpj_beneficiary = search_pattern(pattern, lines[lines.index(line) + 4])

                os.remove(sample)

            dados_boleto = {
                'Layout': 'Boleto',
                'Código Banco': bank_code,
                'Código de Barras': barcode,
                'Local de pagamento': payment_place,
                'Beneficiário': beneficiary,
                'CNPJ Beneficiário': cnpj_beneficiary,
                'Data do Documento': competence,
                'Valor do Documento': value
            }

            if self.dumps:
                print(dumps(dados_boleto, indent=4))

            return dados_boleto

        elif self.layout == 'enel':
            id_client = ''
            name = ''
            address = ''
            competence = ''
            value = ''

            for sample in glob(os.path.join(folder_layout, '*.*')):
                res = ocr_online(sample)

                id_client = re.findall(r'[0-9]{7}', res)[0] if len(re.findall(r'[0-9]{7}', res)) > 0 else ''
                l_dates = list(map(str_to_date, re.findall(r'([0-3][0-9][/][0-1][0-9][/][0-9]{4})', res)))
                competence = str_to_date(min(l_dates), f='%Y-%m-%d', t='%d/%m/%Y') if len(l_dates) > 0 else ''
                l_values = list(map(str_to_float, re.findall(r'(\d+,[0-9]{2})', res)))
                value = '{:.2f}'.format(max(l_values)).replace('.', ',') if len(l_values) > 0 else ''

                lines = res.splitlines()
                for line in lines:
                    if 'Nome' in line:
                        name = line.replace('Nome', '').strip() if name == '' else name
                    elif 'End. Postal' in line:
                        district = re.sub(r'[.\-/]', '', lines[lines.index(line) + 1]).strip()
                        city = re.sub(r'[.\-/]', '', lines[lines.index(line) + 2]).strip()
                        cep = re.sub(r'[.\-/]', '', lines[lines.index(line) + 3]).strip()
                        if address == '':
                            address = '{address}{district}{city}{cep}' \
                                .format(address=line.replace('End. Postal', '').strip(),
                                        district=' - {0}'.format(district) if district != '' else '',
                                        city=' - {0}'.format(city) if city != '' else '',
                                        cep=' - {0}'.format(search_pattern(r'[0-9]{8}', cep)))

                os.remove(sample)

            dados_enel = {
                'Layout': 'Enel',
                'Nº do Cliente': id_client,
                'Nome': name,
                'Endereço': address,
                'Competência': competence,
                'Valor': value
            }

            if self.dumps:
                print(dumps(dados_enel, indent=4))

            return dados_enel

        else:
            return 'Please select some template of personal document or payment title!'


if __name__ == '__main__':
    model = Template('01331100000163', 'enel', dump=True)
    model.execute()
