from ast import literal_eval
from decimal import Decimal

from flask import *
from sqlalchemy.exc import IntegrityError
from sqlalchemy import and_
from unidecode import unidecode

from base import app, path
from models import *
# from cleaner_deamon import run
from layouts import *
from util.yandex import Yandex


# from threading import Thread


@app.template_filter('unidecode')
def transform(name):
    """ Remove all accents of a string """
    return re.sub('[.-/]', '', unidecode(name).lower()).replace(' ', '_')


@app.route('/', methods=['GET', 'POST'])
def index():
    """ Index of website application """
    if request.method == 'POST':
        layout = request.form.get('layout')
        client = request.form.get('client')[:14]
        frequency = request.form.get('frequency')
        session['client'] = client

        Client.query.filter_by(cnpj=client).update({'cleaning_frequency_in_days': frequency})
        db.session.commit()

        return redirect(url_for('dropzone', layout=layout))

    return render_template('index.html', clients=Client.query, layouts=Layout.query)


@app.route('/dropzone/<int:layout>', methods=['GET', 'POST'])
def dropzone(layout):
    q = Layout.query.filter_by(id=layout).first()
    session['layout'] = q.name.lower()

    if request.method == 'POST':
        procedure = Template(session.get('client'), session['layout'])
        result_p = procedure.execute()
        if isinstance(result_p, dict):
            session['data'] = result_p
        else:
            flash(result_p, 'danger')
            return render_template('dropzone.html', message=q.message, multi=q.multiple, layout=q)

        return redirect(url_for('result'))

    return render_template('dropzone.html', message=q.message, multi=q.multiple, layout=q)


@app.route('/upload', methods=['POST'])
def handle_upload():
    layout = session.get('layout')
    client = session.get('client')
    uploaded_path = app.config['UPLOADED_PATH']
    folder_client = os.path.join(uploaded_path, client)
    folder_layout = os.path.join(folder_client, layout)

    for key, f in request.files.items():
        if key.startswith('file'):
            if_not_exist_create(uploaded_path, folder_client, folder_layout)
            f.save(path.join(folder_layout, f.filename))
    return '', 204


@app.route('/result', methods=['GET', 'POST'])
def result():
    data = session.get('data')
    client = session.get('client')

    if not isinstance(data, dict):
        return redirect(url_for('index'))

    if request.method == 'POST':
        s = db.session()

        rejected = []
        layout = request.form.get('layout')
        for ele in data:
            key = ele
            val = data[ele]
            if key == 'Layout':
                continue
            if isinstance(val, dict):
                for k, v in val.items():
                    var_key = '{node}_{child}'.format(node=transform(key), child=transform(k))
                    var_val = request.form.get(var_key)

                    if var_val == 'R':
                        rejected.append(var_key)
                        exec('{var_key} = None'.format(var_key=var_key))
                    else:
                        if val != '':
                            v = '"{v}"'.format(v=v)
                        else:
                            v = 'None'
                        exec('{var_key} = {v}'.format(var_key=var_key, v=v))
            else:
                var_key = '{node}'.format(node=transform(key))
                var_val = request.form.get(var_key)

                if var_val == 'R':
                    rejected.append(var_key)
                    exec('{var_key} = None'.format(var_key=var_key))
                else:
                    if val != '':
                        val = '"{val}"'.format(val=val)
                    else:
                        val = 'None'
                    exec('{var_key} = {val}'.format(var_key=var_key, val=val))

        ignore = ['data', 'ele', 'k', 'key', 'data', 'v', 'val', 'var_key', 'var_val', 'ignore', 'layout', 's',
                  'rejected', 'client']
        variables = [element for element in locals() if element not in ignore]
        values = []

        for variable in variables:
            try:
                # Case variable is brazilian date, convert to american date for database pattern
                value = datetime.strptime(str(eval(variable)), '%d/%m/%Y').strftime('%Y-%m-%d')
            except ValueError:
                try:
                    # Case variable is brazilian datetime, convert to american date for database pattern
                    value = datetime.strptime(str(eval(variable)), '%d/%m/%Y %H:%M:%S').strftime('%Y-%m-%d')
                except ValueError:
                    # Case variable is american currency, convert to brazilian currency
                    if re.match(r'^\d+?,\d+?$', str(eval(variable))) is not None:
                        value = float(str(eval(variable)).replace(',', '.'))
                    # Case variable is competence with 6 digits, transform to american date for database pattern
                    elif re.search(r'^[0-9]{6}$', str(eval(variable))) is not None:
                        value = datetime.strptime(str(eval(variable)), '%m%Y').strftime('%Y-%m-01')
                    # Case variable is bar code with pattern or 47 digits no space
                    elif re.match(r'([0-9]{5}[.]?[0-9]{5}[\s]?[0-9]{5}[.]?[0-9]{6}[\s]?[0-9]{5}[.][0-9]{6}[\s]?'
                                  r'[0-9][\s]?[0-9]{14})|([0-9]{47})', str(eval(variable))) is not None:
                        value = re.sub(r'[\s.\-/]', '', str(eval(variable)))
                    # When any condition was true, so remove slashes, hyphens and dots
                    else:
                        value = re.sub(r'[.\-/]', '', str(eval(variable)))

            values.append(value)

        try:
            last_insert = eval('{model}{values}'.format(model=layout.capitalize(), values=tuple(values)))
            s.add(last_insert)
            s.commit()
            s.flush()
            layout_id = last_insert.id
        except IntegrityError as err:
            s.rollback()
            error = str(err).split('\'')
            err_v = error[1]
            err_k = error[3]
            layout_id = eval('{model}.query.filter_by({field}="{value}").first().id'.format(model=layout.capitalize(),
                                                                                            field=err_k, value=err_v))

        category = Layout.query.filter_by(name=layout).first().category
        if category == 'D':
            s.add(Document(layout.upper(), client, layout_id))
        elif category == 'P':
            competence = eval('{model}.query.filter_by(id={id}).first().competence'.format(model=layout.capitalize(),
                                                                                           id=layout_id))
            amount = eval('{model}.query.filter_by(id={id}).first().value'.format(model=layout.capitalize(),
                                                                                  id=layout_id))
            s.add(Payment(competence, amount, layout.upper(), client, layout_id))
        else:
            return 'Category undefined!'

        try:
            s.commit()
        except IntegrityError as err:
            s.rollback()

        s.close()

        # TODO: Add function of redo capture, when zone of layout rejected

        # When happy way!
        return redirect(url_for('summary', plan=layout.lower(), plan_id=layout_id))

    return render_template('result.html', data=data)


@app.route('/summary/<plan>/<plan_id>')
def summary(plan, plan_id):
    client = session.get('client')
    if client is None:
        return redirect(url_for('index'))

    category = Layout.query.filter_by(name=plan.upper()).first().category
    entity = 'Document' if category == 'D' else 'Payment'

    res = eval('{model}.query.join({entity}, {model}.id == {entity}.{entity_lower})'
               '.filter(and_({entity}.client == "{c}", {entity}.{entity_lower} == {id})).first()'
               .format(model=plan.capitalize(), entity=entity, entity_lower=entity.lower(), c=client, id=plan_id))

    data = perfect_eval(to_json(res))

    return render_template('summary.html', template=plan.upper(), data=data)


def perfect_eval(string):
    try:
        ev = literal_eval(string)
        return ev
    except ValueError:
        corrected = "\'{}\'".format(string)
        ev = literal_eval(corrected)
        return ev


def alchemy_encoder(obj):
    """ JSON encoder function for SQLAlchemy special classes """
    if isinstance(obj, (datetime, date)):
        return datetime.strptime(obj.isoformat(), '%Y-%m-%d').strftime('%d/%m/%Y')
    elif isinstance(obj, Decimal):
        return '{:.2f}'.format(obj).replace('.', ',')
    elif isinstance(obj, int):
        return str(obj)
    raise TypeError('#TypeError: Type {} not serializable'.format(type(obj)))


def to_json(template):
    """ Returns a JSON representation of an SQLAlchemy-backed object """
    c_json = {}

    for col in template._sa_class_manager.mapper.mapped_table.columns:
        col_name = col.name.replace('_', ' ')
        translated_column = Yandex().translate(lang='en-pt', text=col_name)

        if str(getattr(template, col.name)) == 'None':
            c_json[translated_column] = ''
        else:
            c_json[translated_column] = getattr(template, col.name)

    return dumps(c_json, default=alchemy_encoder)


# @app.route("/extract", methods=['GET', 'POST'])
# def extract():
#     if request.method == 'POST':
#         client = request.form.get('client')
#         # begin = request.form.get('begin')
#         # end = request.form.get('end')
#         session['client'] = client
#
#         # ts = s.query(Transaction.data, Transaction.memo, Transaction.valor, Transaction.cnpj,
#         #              func.group_concat(Tag.nome).label('tags')) \
#         #     .outerjoin(TransactionTag, Transaction.id == TransactionTag.transaction_id) \
#         #     .outerjoin(Tag, TransactionTag.tag_id == Tag.id) \
#         #     .filter(and_(Transaction.data.between(ini, fim),
#         #                  Transaction.cnpj == empresa, Tag.nome.isnot(None))).group_by(Transaction.id)
#         #
#         # empresa = Client.query.filter_by(cnpj=empresa).first()
#
#         # if ts.count() > 0:
#         #     ini = datetime.strptime(begin, '%Y-%m-%d').strftime('%d/%m/%Y')
#         #     fim = datetime.strptime(end, '%Y-%m-%d').strftime('%d/%m/%Y')
#         #     return render_template('summary.html', transactions=ts, export=True, emp=empresa, pini=ini, pfim=fim)
#         # else:
#         #     flash('Consulta não retornou nenhum registro!', 'warning')
#         #     return redirect(url_for('extract'))
#
#     return render_template('extract.html', clients=Client.query)


if __name__ == '__main__':
    # Init thread of daemon cleaner
    # Thread(target=run, name='Cleaning Service').start()
    app.run(debug=True)
