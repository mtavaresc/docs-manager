from sqlalchemy.exc import IntegrityError

from base import app
from models import db, Client, Layout


def populate():
    s = db.session
    pre = 'Arraste o(s) arquivo(s) de imagem, <b>que contém '
    suf = '</b>, ou clique para procurar'
    with app.app_context():
        s.add(Client('01331100000163', 'JERONIMOCAR AUTOCENTER COMERCIO E SERVIÇOS EIRELI', cleaning_frequency=30))

        s.add(Layout('Boleto', '{0}as informaçōes do título Boleto{1}'.format(pre, suf), 'P'))
        s.add(Layout('Cagece', '{0}os Dados do Cliente e de Pagamento do título Cagece{1}'.format(pre, suf), 'P', True))
        s.add(Layout('CFe', '{0}as informaçōes do título CF-e{1}'.format(pre, suf), 'P'))
        s.add(Layout('CLT', '{0}as informaçōes do documento CLT{1}'.format(pre, suf), 'D'))
        s.add(Layout('CNH', '{0}as informaçōes do documento CNH{1}'.format(pre, suf), 'D'))
        s.add(Layout('CPF', '{0}as informações do documento CPF{1}'.format(pre, suf), 'D'))
        s.add(Layout('Enel', '{0}as informações do título Enel{1}'.format(pre, suf), 'P'))
        s.add(Layout('RG', '{0}as informações do documento RG{1}'.format(pre, suf), 'D'))

        try:
            s.commit()
        except IntegrityError as err:
            s.rollback()
            if 'Duplicate entry' in str(err):
                return 'Error, object already exists!\n-> {error}'.format(error=err)

    s.close()
